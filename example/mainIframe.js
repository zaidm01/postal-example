/*global postal*/

postal.instanceId( "parent" );

postal.fedx.addFilter( [
	{ channel: 'postal', topic: '#', direction: 'both' },
	{ channel: 'iframez', topic: '#', direction: 'out' },
	{ channel: 'parentz', topic: '#', direction: 'in' },
	{ channel: 'fxc.epi', topic: '#', direction: 'out' }, 
    { channel: 'fxc.epi', topic: '#', direction: 'in' }
] );
postal.addWireTap( function( d, e ) {
	console.log("add wire tap");
	console.log( "ID: " + postal.instanceId() + " " + JSON.stringify( e, null, 4 ) );
} );

$( function() {

	postal.publish( {
		channel: "fxc.epi",
		topic: "transitionToStep",
		data: '2'
	} );

	postal.subscribe( {
		channel: "parentz",
		topic: "#",
		callback: function( d, e ) {
			$( "#msgs" ).append( "<div><pre>" + JSON.stringify( e, null, 4 ) + "</pre></div>" );
		}
	} );

	postal.subscribe( {
		channel: "postal",
		topic: "#",
		callback: function( d, e ) {
			$( "#msgs" ).append( "<div><pre>" + JSON.stringify( e, null, 4 ) + "</pre></div>" );
		}
	} );

	$( "#msg1" ).on( 'click', function() {
		postal.publish( {
			channel: "iframez",
			topic: "some.topic",
			data: "This message will appear in an iframe"
		} );
	} );

	$( "#msg2" ).on( 'click', function() {
		postal.publish( {
			channel: "postal",
			topic: "some.topic",
			data: "This message will appear in an postal main"
		} );
	} );

	// disconnecting via passing the content window as a target
	$( "#disconnect1" ).on( "click", function() {
		postal.fedx.disconnect( {
			target: document.getElementById( "iframe1" ).contentWindow
		} );
	} );

	// disconnecting via passing the instanceId
	$( "#disconnect2" ).on( "click", function() {
		postal.fedx.disconnect( {
			instanceId: "iframe2"
		} );
	} );

	// disconnecting via passing the instanceId
	$( "#clear" ).on( "click", function() {
		$( "#msgs" ).html( "" );
		postal.publish( {
			channel: "iframez",
			topic: "clear"
		} );
	} );

	$( "#transition3" ).on( 'click', function() {
		postal.publish( {
			channel: "fxc.epi",
			topic: "transitionToStep",
			data: "3"
		} );
	} );

	$( "#transition4" ).on( 'click', function() {
		postal.publish( {
			channel: "fxc.epi",
			topic: "transitionToStep",
			data: "4"
		} );
	} );

	$( "#complete" ).on( 'click', function() {
		postal.publish( {
			channel: "fxc.epi",
			topic: "transactionComplete",
			data: '{"transactionId": "212333"}'
		} );
	} );

	postal.subscribe( {
		channel: "fxc.epi",
		topic: "transitionToStep",
		callback: function( d, e ) {
			$( "#msgs" ).append( "<div><pre>Zaid" + JSON.stringify( e, null, 4 ) + "</pre></div>" );
		}
	} );

	$( "#height" ).on( 'click', function() {
		postal.publish( {
			channel: "fxc.epi",
			topic: "setHeightTo",
			data: '900'
		} );
	} );

} );
